//1- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS
let vector = ["Primer elemento", "Segundo elemento", 3, 4, true, false];

//a. Imprimir en la consola el vector
console.log(vector);

//b. Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log(vector[0]);
console.log(vector[vector.length-1]);

//c. Modificar el valor del tercer elemento
vector[2] = 6;

//d. Imprimir en la consola la longitud del vector
console.log(vector.length);

//e. Agregar un elemento al vector usando "push"
vector.push(6);

//f. Eliminar elemento del final e imprimirlo usando "pop"
console.log(vector.pop());

//g. Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.round(vector.length/2),0,'Elemento medio');

//h. Eliminar el primer elemento usando "shift"
let elementoEliminado = vector.shift();

//i. Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(elementoEliminado);
