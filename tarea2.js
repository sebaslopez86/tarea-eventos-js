//2- Escribir un archivo con extensión .js donde se declare un vector de 6 elementos, usando 3 tipos de datos JS:
let vector = ["Primer elemento", "Segundo elemento", 3, 4, true, false];

//a. Imprimir en la consola cada valor usando "for"
for (i = 0; i < vector.length; i++){
    console.log(vector[i])
}

//b. Idem al anterior usando "forEach"
vector.forEach(i => console.log(i));

//c. Idem al anterior usando "map"
vector.map(function(valor,indice) {
    console.log(vector[indice]);
});

// d. Idem al anterior usando "while"
let w = 0;
while (w < vector.length) {
    console.log(vector[w]);
    w++;
}

//e. Idem al anterior usando "for..of"
let n = 0;
for (i of vector) {
  console.log(vector[n]);
  n++;
}